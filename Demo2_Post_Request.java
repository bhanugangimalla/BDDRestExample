package restassuredTests;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;

public class Demo2_Post_Request {
	
	public static HashMap map = new HashMap();
	
	@BeforeClass
	public void postdata(){
		
		map.put("FirstName", RestUtils.getFirstName());
		map.put("LastName", RestUtils.getLastName());
		
		RestAssured.baseURI = "http://restapi.demoqa.com/customer";
		RestAssured.basePath = "/register";
		}
	
	@Test
	public void testPost()
	{
	given()
	   .contentType("application/json")
	   .body(map)
	   .param("MyName", "Naveen")
	   .header("MyHeader", "Indain")
	   
	   
	 .when()
	  
	   .post()
	   
	  .then()
	    .statusCode(201);
	    //.and()
	    //.body("Successcode", equalTo("Operation_Success"));
	    
	}
}