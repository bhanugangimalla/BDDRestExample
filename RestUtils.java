package restassuredTests;

import org.apache.commons.lang3.RandomStringUtils;

public class RestUtils {
	
	public static String getFirstName() {
		String generatedString = RandomStringUtils.randomAlphabetic(1);
		return ("John"+generatedString);
	}
	
	public static String getLastName() {
		String generatedString = RandomStringUtils.randomAlphabetic(1);
		return ("Kenedy"+generatedString);
	}
	
	public static String empName() {
		String generatedString = RandomStringUtils.randomAlphabetic(2);
		return ("Kenedy"+generatedString);
	}
	
	public static String empSal() {
		String generatedString = RandomStringUtils.randomAlphanumeric(3);
		return (generatedString);
	}
	
	public static String empAge() {
		String generatedString = RandomStringUtils.randomAlphanumeric(3);
		return (generatedString);
	}

}
